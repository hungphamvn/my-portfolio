const webpack = require('webpack');

const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
var HtmlWebpackPlugin = require('html-webpack-plugin');
const InterpolateHtmlPlugin = require('react-dev-utils/InterpolateHtmlPlugin');


module.exports = (env) => {
    const isProduction = env === 'production';
    const ASSET_PATH = process.env.PUBLIC_URL || '';
    return {
        entry: './src/app.js',
        output: {
            path: path.join(__dirname, 'public'),
            filename: 'bundle.js',
            publicPath: ASSET_PATH
        },
        module: {
            rules: [{
                loader: 'babel-loader',
                test: /\.js$/,
                exclude: /node_modules/
            }, {
                test: /\.s?css$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader'
                ]
            }]
        },
        plugins: [
            new MiniCssExtractPlugin({
                filename: 'styles.css'
            }),
            new webpack.DefinePlugin({
                'process.env.ASSET_PATH': JSON.stringify(ASSET_PATH)
            }),
            new HtmlWebpackPlugin({
                inject: true,
                template: path.resolve('public/index.html')
            }),
            new InterpolateHtmlPlugin(HtmlWebpackPlugin, {
                PUBLIC_URL: JSON.stringify(ASSET_PATH)
                // You can pass any key-value pairs, this was just an example.
                // WHATEVER: 42 will replace %WHATEVER% with 42 in index.html.
            })
        ],
        devtool: isProduction ? 'source-amp' : 'inline-source-map',
        devServer: {
            contentBase: path.join(__dirname, 'public'),
            historyApiFallback: true
        }

    }
}
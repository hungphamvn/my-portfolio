import React from 'react';
import { NavLink } from 'react-router-dom';

export default () => {
    const PUBLIC_URL = process.env.ASSET_PATH;
    return (
        < div className="header" >
            <NavLink to="/" activeClassName="is-active">
                <img src={`${PUBLIC_URL}/images/brand-logo.png`} alt="my logo" className="header__logo" />
            </NavLink>
            <div className="header__navbar">
                <NavLink className="home_page" to="/" activeClassName="is-active" exact={true} >Homepage</NavLink>
                <NavLink to="/resume" activeClassName="is-active">Resume</NavLink>
                <NavLink to="/contact" activeClassName="is-active">Contact</NavLink>
            </div>
        </div >
    )
}
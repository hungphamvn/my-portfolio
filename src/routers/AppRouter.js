import React from 'react';
import { BrowserRouter, Route, Switch, Link, NavLink } from 'react-router-dom';
import DashboardPage from '../components/dashboard';
import HelpPage from '../components/help';
import ContactPage from '../components/contact'
import NotFoundPage from '../components/pagenotfound';
import Header from '../components/header';
import ResumePage from '../components/resume';


const AppRouter = () => (
    <BrowserRouter basename={process.env.ASSET_PATH}>
        <div>
            <Header />
            <div className="container">
                <Switch>
                    <Route path="/" component={DashboardPage} exact={true} />
                    <Route path="/resume" component={ResumePage} />
                    <Route path="/contact" component={ContactPage} />
                    <Route component={NotFoundPage} />
                </Switch>
            </div>
        </div>
    </BrowserRouter>
)

export default AppRouter;
import { createStore, combineReducers } from 'redux';
import uuid from 'uuid';

// ADD EXPENSE
const addExpense = (
    {
        description = "",
        note = "",
        amount = 0,
        createdAt = 0
    } = {}) => ({
        type: "ADD_EXPENSE",
        expense: {
            id: uuid(),
            description,
            note,
            amount,
            createdAt
        }
    });

// DELETE EXPENSE
const removeExpenses = ({ id = "" } = {}) => ({
    type: "DELETE_EXPENSE",
    id

})

// EDIT EXPENSE
const updateExpenses = (id, updates) => ({
    type: 'UPDATE_EXPENSE',
    id,
    updates
})

// SET TEXT FILTER
const setTextFilter = (updates) => ({
    type: 'SET_TEXT_FILTER',
    updates
})

//SORT BY AMOUNT

const sortByAmount = () => ({
    type: 'SORT_BY_AMOUNT'
})

//SORT BY DATE
const sortByDate = () => ({
    type: 'SORT_BY_DATE'
})

//SET START DATE
const setStartDate = (startDate = undefined) => ({
    type: 'SET_START_DATE',
    startDate
})

//SET END DATE
const setEndDate = (endDate = undefined) => ({
    type: 'SET_END_DATE',
    endDate
})


const expenseReducerDefaultState = []

const expenseReducer = (state = expenseReducerDefaultState, action) => {
    switch (action.type) {
        case 'ADD_EXPENSE':
            return [
                ...state,
                action.expense
            ]
        case 'UPDATE_EXPENSE':
            return state.map((expense) => {
                if (expense.id === action.id) {
                    return {
                        ...expense,
                        ...action.updates,
                    }
                } else {
                    return expense
                }
            })
        case 'DELETE_EXPENSE':
            return state.filter(({ id }) => id !== action.id)
        default:
            return state;
    }
}

const filtersReducerDefaultState = {
    text: '',
    sortBy: 'date',
    startDate: undefined,
    endDate: undefined
}

const filtersReducer = (state = filtersReducerDefaultState, action) => {
    switch (action.type) {
        case 'SET_TEXT_FILTER':
            return {
                ...state,
                ...action.updates
            };
        case 'SORT_BY_AMOUNT':
            return {
                ...state,
                sortBy: 'Amount'
            }
        case 'SORT_BY_DATE':
            return {
                ...state,
                sortBy: 'Date'
            }
        case 'SET_START_DATE':
            return {
                ...state,
                startDate: action.startDate

            }
        case 'SET_END_DATE':
            return {
                ...state,
                endDate: action.endDate

            }
        default:
            return state;
    }
}

const getVisibleExpenses = (expenses, { text, sortBy, startDate, endDate } = {}) => {
    return expenses.filter((expense) => {
        const startDateMatch = typeof startDate !== 'number' || expense.createdAt >= startDate;
        const endDateMatch = typeof endDate !== 'number' || expense.createdAt <= endDate;
        const textMatch = typeof text !== 'string' || expense.description.toLowerCase().includes(text.toLowerCase());
        console.log(startDateMatch + "-" + endDateMatch + "-" + textMatch);
        return startDateMatch && endDateMatch && textMatch;
    }).sort((a, b) => {
        if (sortBy === 'date') {
            console.log('date sort');
            return a.createdAt < b.createdAt ? 1 : -1;
        }
        else if (sortBy === 'amount') {
            console.log('amount sort');
            return a.amount > b.amount ? 1 : -1;
        }
    })
}

const store = createStore(
    combineReducers({
        expenses: expenseReducer,
        filters: filtersReducer
    })
);


store.subscribe(() => {
    const state = store.getState();
    console.log(state);
    const visibleExpenses = getVisibleExpenses(state.expenses, state.filters);
    console.log(visibleExpenses);
});

const expenseOne = store.dispatch(addExpense({ description: "Rent", note: "Rental of January", amount: 250, createdAt: -1500 }))
const expenseTwo = store.dispatch(addExpense({ description: "Coffee", note: "This morning coffee", amount: 550, createdAt: -1000 }))

// store.dispatch(removeExpenses({ id: expenseTwo.expense.id }))
// store.dispatch(updateExpenses(expenseOne.expense.id, { 'amount': 200 }))

//store.dispatch(setTextFilter({ text: 'fee' }));

store.dispatch(sortByAmount());
store.dispatch(sortByDate());

//store.dispatch(setStartDate(1250));
// store.dispatch(setStartDate());

// store.dispatch(setEndDate(1250));
// store.dispatch(setEndDate());

const demoState = {
    expenses: [{
        id: '398c0f21-6bdd-4803-83cb-4003d288ed6a',
        description: "January Rent",
        note: "No note",
        amount: 54000,
        createdAt: 0
    }],
    filters: {
        text: 'rent',
        sortBy: 'amount',
        startDate: undefined,
        endDate: undefined
    }
}
